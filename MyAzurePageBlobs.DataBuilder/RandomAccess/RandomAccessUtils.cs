
namespace MyAzurePageBlobs.DataBuilder.RandomAccess
{


    public struct RandomAccessHandler
    {
        public long FirstBlobPageId { get; set; }
        public int FirstPageOffset { get; set; }
        public int FirstPageExtraSpace { get; set; }
        public int TotalSizeToExecute { get; set; }
        public int TotalPagesAmount { get; set; }
    }
    
    public static class RandomAccessUtils
    {
        public static RandomAccessHandler GetPrecalculatedFieldsAsync(long offset, long dataLength)
        {

            var result = new RandomAccessHandler
            {
                FirstBlobPageId = MyAzurePageBlobUtils.GetBlobPageNo(offset)
            };
            
            result.FirstPageOffset = (int)result.FirstBlobPageId * MyAzurePageBlobUtils.PageSize;

            result.FirstPageExtraSpace = (int)(offset - result.FirstPageOffset);

            result.TotalPagesAmount = (int)MyAzurePageBlobUtils.CalculateRequiredPagesAmount(result.FirstPageExtraSpace + dataLength);

            result.TotalSizeToExecute = result.TotalPagesAmount * MyAzurePageBlobUtils.PageSize;

            return result;
        }
        
    }
}