using System;
using System.Threading.Tasks;

namespace MyAzurePageBlobs.DataBuilder.RandomAccess
{
    public static class RandomAccessExtensions
    {
        
        public static async ValueTask<int> ReadInt16Async(this RandomAccessDataBuilder dataBuilder, long offset)
        {
            var memory = await dataBuilder.ReadAsync(offset, sizeof(short));
            return BitConverter.ToInt16(memory.Span);
        }
        
        public static async ValueTask<uint> ReadUInt16Async(this RandomAccessDataBuilder dataBuilder, long offset)
        {
            var memory = await dataBuilder.ReadAsync(offset, sizeof(ushort));
            return BitConverter.ToUInt16(memory.Span);
        }

        public static async ValueTask<int> ReadIntAsync(this RandomAccessDataBuilder dataBuilder, long offset)
        {
            var memory = await dataBuilder.ReadAsync(offset, sizeof(int));
            return BitConverter.ToInt32(memory.Span);
        }
        
        public static async ValueTask<uint> ReadUIntAsync(this RandomAccessDataBuilder dataBuilder, long offset)
        {
            var memory = await dataBuilder.ReadAsync(offset, sizeof(uint));
            return BitConverter.ToUInt32(memory.Span);
        }
        
        public static async ValueTask<long> ReadInt64Async(this RandomAccessDataBuilder dataBuilder, long offset)
        {
            var memory = await dataBuilder.ReadAsync(offset, sizeof(long));
            return BitConverter.ToInt64(memory.Span);
        }
        
        public static async ValueTask<ulong> ReadUInt64Async(this RandomAccessDataBuilder dataBuilder, long offset)
        {
            var memory = await dataBuilder.ReadAsync(offset, sizeof(ulong));
            return BitConverter.ToUInt64(memory.Span);
        }
        
        public static ValueTask WriteInt64Async(this RandomAccessDataBuilder dataBuilder, long value, long offset)
        {
            var data = new byte[sizeof(long)];
            BitConverter.TryWriteBytes(data, value);
            return dataBuilder.WriteAsync(offset, data);
        }
        
        public static ValueTask WriteUInt64Async(this RandomAccessDataBuilder dataBuilder, ulong value, long offset)
        {
            var data = new byte[sizeof(ulong)];
            BitConverter.TryWriteBytes(data, value);
            return dataBuilder.WriteAsync(offset, data);
        }
        
        public static ValueTask WriteInt16Async(this RandomAccessDataBuilder dataBuilder, short value, long offset)
        {
            var data = new byte[sizeof(short)];
            BitConverter.TryWriteBytes(data, value);
            return dataBuilder.WriteAsync(offset, data);
        }
        
        public static ValueTask WriteUInt16Async(this RandomAccessDataBuilder dataBuilder, ushort value, long offset)
        {
            var data = new byte[sizeof(ushort)];
            BitConverter.TryWriteBytes(data, value);
            return dataBuilder.WriteAsync(offset, data);
        }


        public static ValueTask WriteIntAsync(this RandomAccessDataBuilder dataBuilder, int value, long offset)
        {
            var data = new byte[sizeof(int)];
            BitConverter.TryWriteBytes(data, value);
            return dataBuilder.WriteAsync(offset, data);
        }
        
        public static ValueTask WriteUIntAsync(this RandomAccessDataBuilder dataBuilder, uint value, long offset)
        {
            var data = new byte[sizeof(uint)];
            BitConverter.TryWriteBytes(data, value);
            return dataBuilder.WriteAsync(offset, data);
        }
        
        public static ValueTask WriteByteAsync(this RandomAccessDataBuilder dataBuilder, byte value, long offset)
        {
            var data = new byte[1];
            data[0] = value;
            return dataBuilder.WriteAsync(offset, data);
        }
        
    }
}