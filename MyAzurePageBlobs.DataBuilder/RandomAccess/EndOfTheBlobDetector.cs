using System;
using System.Threading.Tasks;

namespace MyAzurePageBlobs.DataBuilder.RandomAccess
{
    public static class EndOfTheBlobDetector
    {

        public static async ValueTask<long> DetectLastPositionByBackwardScanAsync(this RandomAccessDataBuilder randomAccessDataBuilder, 
            int readBackwardsPagesAmount, Predicate<byte> isThatOurByte)
        {
            var blobSize = await randomAccessDataBuilder.GetDataSizeAsync();

            if (blobSize == 0)
                return -1;
            

            var chunkSize = readBackwardsPagesAmount * MyAzurePageBlobUtils.PageSize;

            var toPosition = blobSize;
            
            while (toPosition > 0)
            {

                var fromPosition = toPosition - chunkSize;

                if (fromPosition < 0)
                    fromPosition = 0;
                    

                var buffer = await randomAccessDataBuilder.ReadAsync(fromPosition, (int) (toPosition - fromPosition));

                for (var i = buffer.Length - 1; i >= 0; i--)
                {
                    if (isThatOurByte(buffer.Span[i]))
                        return fromPosition + i;
                }

                toPosition = fromPosition;

            }

            return -1;
        }
        
    }
}