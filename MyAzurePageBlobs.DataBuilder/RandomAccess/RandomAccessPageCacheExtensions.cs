using System;
using System.IO;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.PagesCaching;

namespace MyAzurePageBlobs.DataBuilder.RandomAccess
{
    public static class RandomAccessPageCacheExtensions
    {

        public static void ExecuteWriteCacheOperation(this PagesCache pagesCache, 
            MemoryStream writtenData, in RandomAccessHandler dataHandle)
        {
            var buffer = writtenData.GetBuffer();
            pagesCache.Add(dataHandle.FirstBlobPageId, new ReadOnlyMemory<byte>(buffer, 0, dataHandle.TotalSizeToExecute));
        }


        internal static async ValueTask ReadWithCacheAsync(this IAzurePageBlob azurePageBlob, 
            byte[] data, PagesCache pagesCache, RandomAccessHandler dataHandle)
        {
            var missingPages=pagesCache.FillPagesWhichAreInCache(dataHandle.FirstBlobPageId, dataHandle.TotalPagesAmount, data);

            await using var memoryStream = new MemoryStream();
            
            foreach (var (startPageToLoad, pagesAmountToLoad) in missingPages.CombineAndGetPagesToLoad())
            {
                memoryStream.Position = 0;
                await azurePageBlob.ReadAsync(memoryStream, startPageToLoad, pagesAmountToLoad);
                
                var position = (int)startPageToLoad * MyAzurePageBlobUtils.PageSize - dataHandle.FirstPageOffset;

                var buffer = memoryStream.GetBuffer();

                Array.Copy(buffer, 0, data, position, memoryStream.Length);
            }
            
            pagesCache.Add(dataHandle.FirstBlobPageId, data);

        }
        
    }
}