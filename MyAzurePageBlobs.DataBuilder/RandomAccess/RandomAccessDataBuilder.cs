using System;
using System.IO;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.PagesCaching;

namespace MyAzurePageBlobs.DataBuilder.RandomAccess
{
    public class RandomAccessDataBuilder
    {
        private readonly IAzurePageBlob _azurePageBlob;
        private readonly int _oneWriteOperationPages;

        public RandomAccessDataBuilder(IAzurePageBlob azurePageBlob, int oneWriteOperationPages=0)
        {
            _azurePageBlob = azurePageBlob;
            _oneWriteOperationPages = oneWriteOperationPages;
        }

        
        private async ValueTask<MemoryStream> ReadPageAsync(long pageNo, int capacity)
        {
            var pageFromCache = PagesCache?.TryGetPage(pageNo);

            var memoryStream = new MemoryStream(capacity);
            if (pageFromCache == null) 
                await _azurePageBlob.ReadAsync(memoryStream, pageNo, 1);
            else
                memoryStream.Write(pageFromCache);
            
            return memoryStream;
        }

        
        private static MemoryStream MakeNewPageWithExtraSpaceAsync(in RandomAccessHandler handler, int capacity)
        {
            var result =  new MemoryStream(capacity);
            result.Write(MyAzurePageBlobUtils.GetEmptyPageWithZeros().Slice(0, handler.FirstPageExtraSpace));
            return result;
        }

        public ValueTask<long> GetDataSizeAsync() => _azurePageBlob.GetBlobSizeAsync();

        public PagesCache PagesCache { get; private set; }
        

        public RandomAccessDataBuilder EnableCaching(int maxCacheSize)
        {
            PagesCache = new PagesCache(maxCacheSize);
            return this;
        }

        public async ValueTask ResizeIfLessAsync(long newDataSize)
        {
            newDataSize = MyAzurePageBlobUtils.CalculatePageSizeToWrite(newDataSize);
            
            var nowSize = await _azurePageBlob.GetBlobSizeAsync();

            if (nowSize < newDataSize)
                await _azurePageBlob.ResizeBlobAsync(newDataSize);
        }

        public async ValueTask WriteAsync(long offset, ReadOnlyMemory<byte> data, int authResizeIfNotEnoughSpaceRatio = 1)
        {

            var blobSize = await _azurePageBlob.GetBlobSizeAsync();

            var dataHandle = RandomAccessUtils.GetPrecalculatedFieldsAsync(offset, data.Length);
            
            var memoryStream = offset >= blobSize
                ? MakeNewPageWithExtraSpaceAsync(dataHandle, dataHandle.TotalSizeToExecute)
                : await ReadPageAsync(dataHandle.FirstBlobPageId, dataHandle.TotalSizeToExecute);

            memoryStream.Position = dataHandle.FirstPageExtraSpace;
            
            memoryStream.Write(data.Span);
            memoryStream.MakeMemoryStreamReadyForPageBlobWriteOperation();

            memoryStream.Position = 0;

            var buffer = memoryStream.BufferAsReadOnlyMemory();

            await _azurePageBlob.WriteBytesAsync(buffer, dataHandle.FirstBlobPageId, new WriteBytesOptions
            {
                BlobResizeRation = authResizeIfNotEnoughSpaceRatio,
                SplitRoundTripsPageSize = _oneWriteOperationPages
            });

            PagesCache?.ExecuteWriteCacheOperation(memoryStream, dataHandle);

        }

        public async ValueTask<ReadOnlyMemory<byte>> ReadAsync(long offset, int readLength)
        {
            
            var blobSize = await _azurePageBlob.GetBlobSizeAsync();

            var handler = RandomAccessUtils.GetPrecalculatedFieldsAsync(offset, readLength);

            if (handler.FirstPageOffset + handler.TotalSizeToExecute > blobSize)
                throw new Exception(
                    $"Blob is too short to execute that operation. BlobSize: {blobSize}; FirstReadOffset: {handler.FirstPageOffset}");

            if (PagesCache != null)
            {
                var resultFromCache = PagesCache.TryGet(handler.FirstBlobPageId, handler.FirstPageExtraSpace, readLength);
                if (!resultFromCache.IsEmpty)
                    return resultFromCache;
            }

            var result = MyAzurePageBlobUtils.AllocateMemory(handler.TotalSizeToExecute);

            if (PagesCache == null)
            {
                await _azurePageBlob.ReadAsync(result, 0, handler.FirstBlobPageId, handler.TotalPagesAmount);  
            }
            else
            {
                await _azurePageBlob.ReadWithCacheAsync(result, PagesCache, handler);
            }

            return new ReadOnlyMemory<byte>(result, handler.FirstPageExtraSpace, readLength) ;
        }
        
        
        public async ValueTask<byte> ReadByteAsync(long offset)
        {
            
            var blobSize = await _azurePageBlob.GetBlobSizeAsync();

            var handler = RandomAccessUtils.GetPrecalculatedFieldsAsync(offset, 1);

            if (handler.FirstPageOffset + handler.TotalSizeToExecute > blobSize)
                throw new Exception(
                    $"Blob is too short to execute that operation. BlobSize: {blobSize}; FirstReadOffset: {handler.FirstPageOffset}");

            if (PagesCache != null)
            {
                if (PagesCache.TryGetByte(handler.FirstBlobPageId, handler.FirstPageOffset, out var byteResult))
                    return byteResult;
            }

            var result = await ReadAsync(offset, 1);
            return result.Span[0];
        }        
        
        
        
    }
}