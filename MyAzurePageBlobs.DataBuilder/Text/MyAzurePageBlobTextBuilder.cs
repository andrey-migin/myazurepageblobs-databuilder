using System;
using System.Text;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.RandomAccess;

namespace MyAzurePageBlobs.DataBuilder.Text
{
    public class MyAzurePageBlobTextBuilder
    {

        private readonly RandomAccessDataBuilder _randomAccessDataBuilder;

        private readonly IAzurePageBlob _azurePageBlob;
        private readonly int _autoResizePages;

        public MyAzurePageBlobTextBuilder(IAzurePageBlob azurePageBlob, int autoResizePages, int pagesToCache = 512, int maxPagesWriteChunk = 0)
        {
            _azurePageBlob = azurePageBlob;
            _autoResizePages = autoResizePages;
            _randomAccessDataBuilder = new RandomAccessDataBuilder(azurePageBlob, maxPagesWriteChunk)
                .EnableCaching(pagesToCache);
        }

        public long Position { get; private set; } = -1;
 

        private Encoding _encoding = Encoding.UTF8;

        public MyAzurePageBlobTextBuilder SetTextEncoder(Encoding encoding)
        {
            _encoding = encoding;
            return this;
        }

        public async ValueTask InitAsync()
        {
            Position = await _randomAccessDataBuilder.DetectLastPositionByBackwardScanAsync(_autoResizePages,
                b => b != 0)+1;
        }

        public async ValueTask AppendAsync(string text)
        {
            if (Position < 0)
                await InitAsync();
            
            var bytes = _encoding.GetBytes(text);
            await _randomAccessDataBuilder.WriteAsync(Position, bytes);
            Position += bytes.Length;
        }


        public async ValueTask<string> DownloadAsync()
        {
            var result = await _azurePageBlob.DownloadAsync();
            return _encoding.GetString(result.GetBuffer().AsSpan(0, (int)Position));
        }
        
    }
}