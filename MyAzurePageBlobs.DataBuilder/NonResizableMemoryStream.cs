using System;
using System.IO;

namespace MyAzurePageBlobs.DataBuilder
{
    public class NonResizableMemoryStream : Stream
    {
        public Memory<byte> Data { get; }

        public NonResizableMemoryStream(Memory<byte> data)
        {
            Data = data;
        }
        
        public NonResizableMemoryStream(int bufferSize)
        {
            Data = MyAzurePageBlobUtils.AllocateMemory(bufferSize);
        }
        
        public override void Flush()
        {
    
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var copyLen = Data.Length - (int) Position;
            if (copyLen > count)
                copyLen = count;

            Data.Slice((int) Position, copyLen).CopyTo(new Memory<byte>(buffer, offset, copyLen));
            Position += copyLen;
            return copyLen;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            var mem = new Memory<byte>(buffer, offset, count);
            
            mem.CopyTo(Data.Slice((int)Position));
            Position += count;
        }

        private static readonly Memory<byte> Empty = new Memory<byte>();
        
        public Memory<byte> GetRemainingMemory()
        {
            if (Position == Data.Length)
                return Empty;

            var pos = (int) Position;
            return Data.Slice(pos, Data.Length - pos);
        }

        public override bool CanRead { get; } = true;
        public override bool CanSeek { get; } = false;
        public override bool CanWrite { get; } = false;
        public override long Length => Data.Length;
        public override long Position { get; set; }
    }
}