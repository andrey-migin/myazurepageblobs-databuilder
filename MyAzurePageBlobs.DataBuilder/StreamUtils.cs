using System;
using System.Collections.Generic;
using System.IO;

namespace MyAzurePageBlobs.DataBuilder
{
    internal static class StreamExtensions
    {


        public static ReadOnlyMemory<byte> BufferAsReadOnlyMemory(this MemoryStream memoryStream)
        {
            var buffer = memoryStream.GetBuffer();

            return new ReadOnlyMemory<byte>(buffer, 0, (int)memoryStream.Length);

        }
        
        
        public static byte[] ToArray(this Stream src)
        {
            switch (src)
            {
                case MemoryStream memoryStream:
                    return memoryStream.ToArray();
                
                case NonResizableMemoryStream readOnlyMemoryStream:
                    return readOnlyMemoryStream.Data.ToArray();
            }

            var resultStream = new MemoryStream();
            src.Position = 0;
            src.CopyTo(resultStream);
            return resultStream.ToArray();
        }

        public static void WriteInt(this Stream src, int value)
        {
            Span<byte> array = stackalloc byte[sizeof(int)];
            BitConverter.TryWriteBytes(array, value);
            src.Write(array);
        }


        public static IEnumerable<ReadOnlyMemory<byte>> SplitIt(this ReadOnlyMemory<byte> srcData, int maxChunkSize)
        {
            var pos = 0;
            var remains = srcData.Length;

            while (remains>0)
            {
                var chunkSize = remains > maxChunkSize ? maxChunkSize : remains;
                yield return srcData.Slice(pos, chunkSize);

                pos += chunkSize;
                remains -= chunkSize;
            }

        } 
        
    }
}