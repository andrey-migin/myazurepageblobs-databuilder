using System.Collections.Generic;

namespace MyAzurePageBlobs.DataBuilder.PagesCaching
{
    public static class PagesCacheUtils
    {

        public static IEnumerable<(long startPageToLoad, int pagesAmountToLoad)> CombineAndGetPagesToLoad(
            this IEnumerable<long> missingPages)
        {

            long startPageId = -1;
            long prevPageId = -5;
            var count = 0;

            foreach (var missingPage in missingPages)
            {
                if (missingPage - 1 == prevPageId)
                {
                    count++;
                }
                else
                {
                    if (count >0)
                        yield return (startPageId, count);

                    startPageId = missingPage;
                    count = 1;
                }

                prevPageId = missingPage;
            }

            if (startPageId != -1 && count > 0)
                yield return (startPageId, count);

        }
        
    }
}