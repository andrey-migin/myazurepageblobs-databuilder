using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MyAzurePageBlobs.DataBuilder.PagesCaching
{
    public class PagesCache
    {

        public class CacheItem
        {
            private byte[] _data;

            public CacheItem(byte[] data, long pageId)
            {
                _data = data;
                PageId = pageId;
                LastAccess = DateTime.UtcNow;
            }

            public byte[] GetData()
            {
                LastAccess = DateTime.UtcNow;
                return _data;
            }

            public void Update(byte[] data)
            {
                _data = data;
                LastAccess = DateTime.UtcNow;
            }
            
            public DateTime LastAccess { get; private set; }
            
            public long PageId { get; }

            public override string ToString()
            {
                return "Page: " + PageId+" with access time: "+LastAccess.ToString("O");
            }
        }
        
        private readonly int _maxPagesSize;


        private readonly Dictionary<long, CacheItem> _cacheData
            = new Dictionary<long, CacheItem>();

        private readonly ReaderWriterLockSlim _lockSlim
            = new ReaderWriterLockSlim();


        public PagesCache(int maxPagesSize)
        {
            
            _maxPagesSize = maxPagesSize;
        }

        
        //ToDo - Create UnitTests on GC
        private void GarbageCollect()
        {
            List<CacheItem> indexByDateTime = null; 
            
            while (_cacheData.Count > _maxPagesSize)
            {
                indexByDateTime ??= _cacheData.Values.OrderByDescending(itm => itm.LastAccess).ToList();

                var itemToDelete = indexByDateTime[^1];

                _cacheData.Remove(itemToDelete.PageId);
                
                indexByDateTime.RemoveAt(indexByDateTime.Count-1);
            }
        }
        

        public void Add(long fromPageId, ReadOnlyMemory<byte> data)
        {
            _lockSlim.EnterWriteLock();
            try
            {
                var pageId = fromPageId;
                foreach (var theChunk in data.SplitIt(MyAzurePageBlobUtils.PageSize))
                {
                    if (!_cacheData.ContainsKey(pageId))
                        _cacheData.Add(pageId, new CacheItem(theChunk.ToArray(), pageId));
                    else
                        _cacheData[pageId].Update(theChunk.ToArray());

                    pageId++;
                }   
                
                GarbageCollect();
            }
            finally
            {
                _lockSlim.ExitWriteLock();
            }
        }

        public byte[] TryGetPage(long pageNo)
        {
            _lockSlim.EnterReadLock();
            try
            {
                return _cacheData.ContainsKey(pageNo) ? _cacheData[pageNo].GetData() : null;
            }
            finally
            {
                _lockSlim.ExitReadLock();
            }
        }

        public IReadOnlyList<long> FillPagesWhichAreInCache(long startPageId, int pagesAmount, Memory<byte> bufferToFill)
        {

            var missingPages = new List<long>();
            
            _lockSlim.EnterReadLock();
            try
            {

                var pos = 0;

                for (var pageId = startPageId; pageId < startPageId + pagesAmount; pageId++)
                {
                    if (_cacheData.ContainsKey(pageId))
                    {
                        var item = _cacheData[pageId];

                        var pageData = item.GetData();
                        pageData.AsSpan().CopyTo(bufferToFill.Span.Slice(pos, MyAzurePageBlobUtils.PageSize));
                    }
                    else
                    {
                        missingPages.Add(pageId);
                    }

                    pos += MyAzurePageBlobUtils.PageSize;
                }
            }
            finally
            {
                _lockSlim.ExitReadLock();
            }

            return missingPages;
        }


        private byte[] CompileFromTwoPages(long startPageNo, int pageOffset, int length)
        {

            if (!_cacheData.ContainsKey(startPageNo + 1))
                return default;

            var firstPage = _cacheData[startPageNo].GetData();
            var secondPage = _cacheData[startPageNo+1].GetData();
            
            
            var result = MyAzurePageBlobUtils.AllocateMemory(length);
            var firstChunkLen = MyAzurePageBlobUtils.PageSize - pageOffset;
            
            Array.Copy(firstPage, pageOffset, result, 0, firstChunkLen);
            Array.Copy(secondPage, 0, result, firstChunkLen, length - firstChunkLen);

            return result;

        }

        public ReadOnlyMemory<byte> TryGet(long startPageNo, int pageOffset, int length)
        {
            
            _lockSlim.EnterReadLock();
            try
            {
                if (length > MyAzurePageBlobUtils.PageSize)
                    return default;

                if (!_cacheData.ContainsKey(startPageNo))
                    return default;

                if (pageOffset + length <= MyAzurePageBlobUtils.PageSize)
                {
                    var page = _cacheData[startPageNo].GetData();
                    return new ReadOnlyMemory<byte>(page, pageOffset, length);
                }

                return CompileFromTwoPages( startPageNo,  pageOffset, length);

            }
            finally
            {
                _lockSlim.ExitReadLock();
            }
            
            
        }
        
        public bool TryGetByte(long startPageNo, int pageOffset, out byte value)
        {
            
            _lockSlim.EnterReadLock();
            try
            {
                if (!_cacheData.ContainsKey(startPageNo))
                {
                    value = 0;
                    return false;
                }
                    
                
                var page = _cacheData[startPageNo].GetData();

                value = page[pageOffset];
                return true;
            }
            finally
            {
                _lockSlim.ExitReadLock();
            }
            
            
        }
        
        
    }
}