using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.RandomAccess;

namespace MyAzurePageBlobs.DataBuilder.Json
{
    
    public class MyAzurePageBlobJsonArrayBuilder
    {
        private readonly int _autoResizePages;
        private readonly RandomAccessDataBuilder _randomAccessDataBuilder;

        public long Position { get; private set; } = -1;

        private readonly IAzurePageBlob _azurePageBlob;

        public MyAzurePageBlobJsonArrayBuilder(IAzurePageBlob azurePageBlob, int pagesInCache, 
            int autoResizePages, int maxPagesWriteChunk = 0)
        {
            _azurePageBlob = azurePageBlob;
            _autoResizePages = autoResizePages;
            _randomAccessDataBuilder = new RandomAccessDataBuilder(azurePageBlob, maxPagesWriteChunk)
                .EnableCaching(pagesInCache);
        }


        private Action<object, Stream> _jsonSerializer;

        public MyAzurePageBlobJsonArrayBuilder SetJsonObjectSerializer(Action<object, Stream> jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
            return this;
        }



        private async Task AppendToBlobAsync(ReadOnlyMemory<byte> content)
        {
            await _randomAccessDataBuilder.WriteAsync(Position, content, _autoResizePages);
            Position += content.Length - 1;
        }
        
        

        public async ValueTask InitAsync()
        {
            Position
                = await _randomAccessDataBuilder.DetectLastPositionByBackwardScanAsync(_autoResizePages,
                    b => b == ClosedBracket);
            
            if (Position>0)
                return;

            Position++;
            await AppendToBlobAsync(_emptyContent.Value);
        }
        

        public async ValueTask AppendAsync(object data)
        {
            if (_jsonSerializer ==null)
                throw new Exception("Please Set JsonToObject Serializer");

            if (Position < 0)
                await InitAsync();

            var stream = new MemoryStream();
            if (Position > 1)
                stream.WriteByte((byte) ',');
            
            _jsonSerializer(data, stream);
            
            stream.WriteByte(ClosedBracket);

            await AppendToBlobAsync(stream.BufferAsReadOnlyMemory());

        }
        
        public async ValueTask AppendBulkAsync(IEnumerable<object> objects)
        {
            if (_jsonSerializer ==null)
                throw new Exception("Please Set JsonToObject Serializer");

            if (Position < 0)
                await InitAsync();
            
            var stream = new MemoryStream();

            var secondItem = false;
            
            foreach (var o in objects)
            {
                if (Position > 1 || secondItem)
                    stream.WriteByte((byte) ',');
                
                secondItem = true;
                _jsonSerializer(o, stream);
            }
            
            stream.WriteByte(ClosedBracket);

            await AppendToBlobAsync(stream.BufferAsReadOnlyMemory());
        }

        
        private const byte OpenBracket = (byte) '[';
        private const byte ClosedBracket = (byte) ']';

        private readonly Lazy<byte[]> _emptyContent = new Lazy<byte[]>(() =>
        {
            var result = new byte[2];
            result[0] = OpenBracket;
            result[1] = ClosedBracket;
            return result;
        });

        public async Task<ReadOnlyMemory<byte>> DownloadAsync()
        {
            var result = await _azurePageBlob.DownloadAsync();
            var buffer = result.GetBuffer();

            return new ReadOnlyMemory<byte>(buffer, 0, (int)Position+1);
        }
    }
    
}