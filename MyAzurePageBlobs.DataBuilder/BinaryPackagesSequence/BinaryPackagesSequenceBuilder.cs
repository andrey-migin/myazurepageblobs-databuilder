using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.RandomAccess;

namespace MyAzurePageBlobs.DataBuilder.BinaryPackagesSequence
{
    public class BinaryPackagesSequenceBuilder
    {
        
        
        private readonly IAzurePageBlob _azurePageBlob;
        private readonly int _autoScalePages;

        public long Position { get; private set; } = -1;

        private readonly RandomAccessDataBuilder _randomAccessDataBuilder;
        
        public BinaryPackagesSequenceBuilder(IAzurePageBlob azurePageBlob, int pagesAmountToCache, int autoScalePages, int maxPagesWriteChunk = 0)
        {
            _azurePageBlob = azurePageBlob;
            _autoScalePages = autoScalePages;
            _randomAccessDataBuilder = new RandomAccessDataBuilder(azurePageBlob, maxPagesWriteChunk)
                .EnableCaching(pagesAmountToCache);
        }

        public async Task InitAsync(int pagesAmountToScan)
        {
            await foreach (var _ in InitAndReadAsync(pagesAmountToScan))
            {
                
            }
        }


        public async IAsyncEnumerable<ReadOnlyMemory<byte>> InitAndReadAsync(int pagesAmountToScan)
        {
            var binaryPackageReader = new BinaryPackageReader();

            Position = 0;

            await foreach (var frame in _azurePageBlob.ReadPagesAsync(pagesAmountToScan))
            {
                binaryPackageReader.AddFrame(frame);

                while (binaryPackageReader.NextDataMarker != NextDataMarker.PleaseReadMore)
                {
                    switch (binaryPackageReader.NextDataMarker)
                    {
                        case NextDataMarker.EndOfData:
                            yield break;

                        case NextDataMarker.FrameIsReady:
                        {
                            var chunk = binaryPackageReader.GetNextDataChunk();
                            yield return chunk;
                            Position += chunk.Length + BinaryPackageReader.IntLen;
                            break;
                        }
                    }
                }

            }
        }


        private readonly byte[] _endMarker = new byte[sizeof(int)];

        
        public async ValueTask AppendAsync(ReadOnlyMemory<byte> packageToAppend)
        {

            if (Position < 0)
                await InitAsync(_autoScalePages);
            
            var stream = new MemoryStream();
            stream.WriteInt(packageToAppend.Length);
            stream.Write(packageToAppend.Span);
            stream.Write(_endMarker);

            var memoryToWrite = stream.BufferAsReadOnlyMemory();

            await _randomAccessDataBuilder.WriteAsync(Position, memoryToWrite, _autoScalePages);

            Position += memoryToWrite.Length - _endMarker.Length;

        }
        
        
        public async ValueTask AppendAsync(IEnumerable<ReadOnlyMemory<byte>> packagesToAppend)
        {      
            if (Position < 0)
                await InitAsync(_autoScalePages);
            
            var stream = new MemoryStream();
            
            foreach (var packageToAppend in packagesToAppend)
            {
                stream.WriteInt(packageToAppend.Length);
                stream.Write(packageToAppend.Span);
            }
    
            stream.Write(_endMarker);
            
            var memoryToWrite = stream.BufferAsReadOnlyMemory();

            await _randomAccessDataBuilder.WriteAsync(Position, memoryToWrite, _autoScalePages);
            
            Position += memoryToWrite.Length - _endMarker.Length;
        }
        
        public async Task<ReadOnlyMemory<byte>> DownloadAsync()
        {
            var result = await _azurePageBlob.DownloadAsync();
            var buffer = result.GetBuffer();

            return new ReadOnlyMemory<byte>(buffer, 0, (int)Position+_endMarker.Length);
        }
    
    }
    
}