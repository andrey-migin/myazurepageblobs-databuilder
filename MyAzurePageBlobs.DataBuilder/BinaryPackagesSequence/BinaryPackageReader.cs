using System;
using System.Collections.Generic;

namespace MyAzurePageBlobs.DataBuilder.BinaryPackagesSequence
{


    public enum NextDataMarker
    {
        NotInitialized, PleaseReadMore, FrameIsReady, EndOfData
    }
    
    public class BinaryPackageReader
    {
        public const int IntLen = sizeof(int);

        private readonly List<ReadOnlyMemory<byte>> _frames = new List<ReadOnlyMemory<byte>>();
        
        public int DataLength { get; private set; }

        public void AddFrame(byte[] newFrame)
        {
            _frames.Add(newFrame);
            DataLength += newFrame.Length;
            NextDataMarker = CompileNexDataMarker();

        }

        private int _nextDataChunkSize = -1;
                
        private void ReduceFirstFrameSize(int size)
        {
            DataLength -= size;
            
            if (_frames[0].Length == size)
            {
                _frames.RemoveAt(0);
                return;
            }
            
            _frames[0] = _frames[0].Slice(size, _frames[0].Length - size);
        }


        private int GetNextDataChunkFromSingleFrame()
        {
            var result = BitConverter.ToInt32(_frames[0].Span);
            ReduceFirstFrameSize(IntLen);
            return result; 
        }
        

        private int GetNextDataChunkSize()
        {
            if (_frames[0].Length >= IntLen)
                return GetNextDataChunkFromSingleFrame();

            Span<byte> temporaryArray = stackalloc byte[IntLen];

            var framePosition = 0;

            for (var i = 0; i < IntLen; i++)
            {
                temporaryArray[0] = _frames[0].Span[framePosition];

                framePosition++;
                if (framePosition >= _frames[0].Length)
                {
                    _frames.RemoveAt(0);
                    framePosition = 0;
                } 
            }

            return BitConverter.ToInt32(temporaryArray);
        }


        private ReadOnlyMemory<byte> CompileNextDataChunk()
        {
            if (_nextDataChunkSize <= _frames[0].Length)
            {
                var resultAsOnePiece =  _nextDataChunkSize == _frames.Count ?  _frames[0] : _frames[0].Slice(0, _nextDataChunkSize);
                ReduceFirstFrameSize(_nextDataChunkSize);
                return resultAsOnePiece;
            }

            var result = MyAzurePageBlobUtils.AllocateMemory(_nextDataChunkSize);
            var pos = 0;
            var remainSize = _nextDataChunkSize;

            while (remainSize>0)
            {
                var copySize = _frames[0].Length < remainSize ? _frames[0].Length : remainSize;

                _frames[0].Span.Slice(0, copySize).CopyTo(result.AsSpan(pos, remainSize));
                
                ReduceFirstFrameSize(copySize);

                pos += copySize;
                remainSize -= copySize;
            }

            return result;
        }

        private NextDataMarker CompileNexDataMarker()
        {
            if (DataLength < IntLen)
                return NextDataMarker.PleaseReadMore;

            if (_nextDataChunkSize < 0)
                _nextDataChunkSize = GetNextDataChunkSize();

            if (_nextDataChunkSize == 0)
                return NextDataMarker.EndOfData;

            if (DataLength < _nextDataChunkSize + IntLen)
                return NextDataMarker.PleaseReadMore;

            return NextDataMarker.FrameIsReady;
        }


        public NextDataMarker NextDataMarker { get; private set; } = NextDataMarker.NotInitialized;


        public ReadOnlyMemory<byte> GetNextDataChunk()
        {
            var result = CompileNextDataChunk();
            _nextDataChunkSize = -1;
            NextDataMarker = CompileNexDataMarker();
            return result;
        }


    }
}