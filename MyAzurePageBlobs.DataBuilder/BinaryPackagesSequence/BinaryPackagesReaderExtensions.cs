using System.Collections.Generic;
using System.IO;

namespace MyAzurePageBlobs.DataBuilder.BinaryPackagesSequence
{
    public static class BinaryPackagesReaderExtensions
    {
        /// <summary>
        /// By returning frame position we detect the end of the sequence
        /// </summary>
        /// <param name="azurePageBlob"></param>
        /// <param name="scanPageSize">Size of the Frame we are using to read from Blob in pages</param>
        /// <returns></returns>
        public static async IAsyncEnumerable<byte[]> ReadPagesAsync(this IAzurePageBlob azurePageBlob, int scanPageSize)
        {
            var blobSize = await azurePageBlob.GetBlobSizeAsync();
            
            if (blobSize == 0)
                yield break;

            var pageNo = 0;

            var lastPageNo = blobSize / MyAzurePageBlobUtils.PageSize;

            var remains = lastPageNo;
            

            var memoryStream = new MemoryStream(scanPageSize*MyAzurePageBlobUtils.PageSize);

            while (pageNo<lastPageNo)
            {
                memoryStream.Position = 0;

                var pagesToRead = remains > scanPageSize ? scanPageSize : (int)remains;
                await azurePageBlob.ReadAsync(memoryStream, pageNo, pagesToRead);

                var buffer = memoryStream.GetBuffer();

                var callBackArray = MyAzurePageBlobUtils.AllocateMemory(buffer.Length);
                
                buffer.CopyTo(callBackArray,0);

                yield return callBackArray;

                pageNo += pagesToRead;
                remains -= pagesToRead;
            }

        }  
    }
}