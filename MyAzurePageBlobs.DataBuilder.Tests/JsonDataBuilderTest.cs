using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.Json;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    
    public class JsonDataBuilderTest
    {
        public class MyItem
        {
            public int Value { get; set; }
        }

        [SetUp]
        public void Init()
        {
            MyAzurePageBlobUtils.AllocateMemory = size => GC.AllocateUninitializedArray<byte>(size);
        }

        [Test]
        public async Task TestSimpleObjectSave()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var jsonDataBuilder = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 2)
                .WithNewtonSoftJsonSerializer();

            for (var i = 0; i < 10; i++)
            {
                await jsonDataBuilder.AppendAsync(new MyItem
                {
                    Value = i
                });
            }

            var dataMemory = await jsonDataBuilder.DownloadAsync();

            var json = dataMemory.ToArray();

            var jsonAsText = Encoding.ASCII.GetString(json);

            var resultJson = Newtonsoft.Json.JsonConvert.DeserializeObject<MyItem[]>(jsonAsText);
            
            Assert.AreEqual(10, resultJson.Length);

            var j = 0;
            foreach (var myItem in resultJson)
            {
                Assert.AreEqual(j, myItem.Value);
                j++;
            }

        }
        
        [Test]
        public async Task TestArrayOfObjectsSave()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var jsonDataBuilder = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 2)
                .WithNewtonSoftJsonSerializer();

            var items = new List<object>();
            for (var i = 0; i < 10000; i++)
            {
                items.Add(new MyItem
                {
                    Value = i
                });

            }
            await jsonDataBuilder.AppendBulkAsync(items);
            
            var dataMemory = await jsonDataBuilder.DownloadAsync();

            var json = dataMemory.ToArray();

            var jsonAsText = Encoding.ASCII.GetString(json);
            
            Console.WriteLine(jsonAsText.Substring(0, 64)+".....");
            Console.WriteLine("....."+jsonAsText.Substring(jsonAsText.Length-64, 64));

            var resultJson = Newtonsoft.Json.JsonConvert.DeserializeObject<IReadOnlyList<MyItem>>(jsonAsText);
            
            Assert.AreEqual(10000, resultJson.Count);

            var j = 0;
            foreach (var myItem in resultJson)
            {
                Assert.AreEqual(j, myItem.Value);
                j++;
            }

        }
        
        [Test]
        public async Task TestOpenAndReopen()
        {

   
            var pageBlob = new MyAzurePageBlobInMem();
            var jsonDataBuilder = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 2048)
                .WithNewtonSoftJsonSerializer();
            
            var item0 = new MyItem {Value = 0};
           
            await jsonDataBuilder.AppendAsync(item0);
            
            
            var json = await jsonDataBuilder.DownloadAsync();
            var jsonAsText = Encoding.ASCII.GetString(json.Span);
            
            Console.WriteLine(jsonAsText);
            
            var jsonDataBuilder2 = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 2)
                .WithNewtonSoftJsonSerializer();
            
            var item1 = new MyItem {Value = 1};
            await jsonDataBuilder2.AppendAsync(item1);
            
            ///////////
                    
             json = await jsonDataBuilder2.DownloadAsync();

            jsonAsText = Encoding.ASCII.GetString(json.Span);
            
            Console.WriteLine(jsonAsText.Trim());

            var resultJson = Newtonsoft.Json.JsonConvert.DeserializeObject<IReadOnlyList<MyItem>>(jsonAsText);
            
            var j = 0;
            foreach (var myItem in resultJson)
            {
                Assert.AreEqual(j, myItem.Value);
                j++;
            }

        }

              
        [Test]
        public async Task TestOpenWithBigAmountAndReopen()
        {

   
            var pageBlob = new MyAzurePageBlobInMem();
            var jsonDataBuilder = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 2048)
                .WithNewtonSoftJsonSerializer();

            var firstItems = new List<MyItem>();
            for (var i = 0; i < 512; i++)
            {
                var item = new MyItem {Value = i};  
                firstItems.Add(item);
            }
           
            await jsonDataBuilder.AppendBulkAsync(firstItems);
            
            var json = await jsonDataBuilder.DownloadAsync();
            var jsonAsText = Encoding.ASCII.GetString(json.Span);

            var firstChunkJsonLen = jsonAsText.Length;
            
            Console.WriteLine("First chunk Json Start with Total Length: "+firstChunkJsonLen);
            Console.Write(jsonAsText.Substring(0,64)+"...");
            Console.WriteLine(jsonAsText.GetLastPartOfText());
            
            var jsonDataBuilder2 = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 6)
                .WithNewtonSoftJsonSerializer();
            
            var secondItems = new List<MyItem>();
            for (var i = 512; i < 1024; i++)
            {
                var item = new MyItem {Value = i};  
                secondItems.Add(item);
            }
            await jsonDataBuilder2.AppendBulkAsync(secondItems);
            
            ///////////

            json = await jsonDataBuilder2.DownloadAsync();
            jsonAsText = Encoding.ASCII.GetString(json.Span);
            Console.WriteLine("Second chunk Json Start with Total Length: "+jsonAsText.Length);
            Console.Write(jsonAsText.GetFirstPartOfText());
            Console.WriteLine(jsonAsText.GetLastPartOfText());
            Console.WriteLine(jsonAsText.GetTheMiddleOfTheText(firstChunkJsonLen));
            
            var resultJson = Newtonsoft.Json.JsonConvert.DeserializeObject<IReadOnlyList<MyItem>>(jsonAsText);
            
            Assert.AreEqual(1024, resultJson.Count);
            
            var j = 0;
            foreach (var myItem in resultJson)
            {
                Assert.AreEqual(j, myItem.Value);
                j++;
            }

        }
        
        [Test]
        public async Task TestOpenWithBigAmountAndReopenWithSmallCache()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var jsonDataBuilder = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 2048)
                .WithNewtonSoftJsonSerializer();

            var firstItems = new List<MyItem>();
            for (var i = 0; i < 512; i++)
            {
                var item = new MyItem {Value = i};  
                firstItems.Add(item);
            }
           
            await jsonDataBuilder.AppendBulkAsync(firstItems);
            
            var json = await jsonDataBuilder.DownloadAsync();
            var jsonAsText = Encoding.ASCII.GetString(json.Span);

            var firstChunkJsonLen = jsonAsText.Length;
            
            Console.WriteLine("First chunk Json Start with Total Length: "+firstChunkJsonLen);
            Console.Write(jsonAsText.Substring(0,64)+"...");
            Console.WriteLine(jsonAsText.GetLastPartOfText());
            
            var jsonDataBuilder2 = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 512, 6)
                .WithNewtonSoftJsonSerializer();
            
            var secondItems = new List<MyItem>();
            for (var i = 512; i < 1024; i++)
            {
                var item = new MyItem {Value = i};  
                secondItems.Add(item);
            }
            await jsonDataBuilder2.AppendBulkAsync(secondItems);
            
            ///////////

            json = await jsonDataBuilder2.DownloadAsync();
            jsonAsText = Encoding.ASCII.GetString(json.Span);
            Console.WriteLine("Second chunk Json Start with Total Length: "+jsonAsText.Length);
            Console.Write(jsonAsText.GetFirstPartOfText());
            Console.WriteLine(jsonAsText.GetLastPartOfText());
            Console.WriteLine(jsonAsText.GetTheMiddleOfTheText(firstChunkJsonLen));
            
            var resultJson = Newtonsoft.Json.JsonConvert.DeserializeObject<IReadOnlyList<MyItem>>(jsonAsText);
            
            Assert.AreEqual(1024, resultJson.Count);
            
            var j = 0;
            foreach (var myItem in resultJson)
            {
                Assert.AreEqual(j, myItem.Value);
                j++;
            }

        }


        [Test]
        public async Task TestInitEmptyJsonBuilder()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var jsonDataBuilder = new MyAzurePageBlobJsonArrayBuilder(pageBlob, 2048, 2048)
                .WithNewtonSoftJsonSerializer();

            await jsonDataBuilder.InitAsync();
            
            Assert.AreEqual(1, jsonDataBuilder.Position);
        }
        
        
    }
    
}