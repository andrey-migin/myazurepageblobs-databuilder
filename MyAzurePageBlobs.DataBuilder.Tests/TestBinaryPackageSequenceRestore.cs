using System;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.BinaryPackagesSequence;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class TestBinaryPackageSequenceRestore
    {
        [SetUp]
        public void Init()
        {
            MyAzurePageBlobUtils.AllocateMemory = size => GC.AllocateUninitializedArray<byte>(size);
        }
        

        [Test]
        public async Task TestBasicCase()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);

            await binaryDataBuilder.AppendAsync(new byte[] {1, 2, 3});
            await binaryDataBuilder.AppendAsync(new byte[] {4, 5, 6});

            
            binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            var restoredItems = await binaryDataBuilder.InitAndReadAsync(1).ToListAsync();

            Assert.AreEqual(2, restoredItems.Count);
            
            await binaryDataBuilder.AppendAsync(new byte[] {7, 8, 9});
            
            binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            restoredItems = await binaryDataBuilder.InitAndReadAsync(1).ToListAsync();
            
            Assert.AreEqual(3, restoredItems.Count);
            
        }


        [Test]
        public async Task TestThatPositionIsTheSameAfterRestart()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            

            var firstPackage = new byte[520];
            
            Array.Fill<byte>(firstPackage, 5);
            
            await binaryDataBuilder.AppendAsync(firstPackage);

            var array2 = new byte[] {4, 5, 6};
            await binaryDataBuilder.AppendAsync(array2);

            var positionBefore = binaryDataBuilder.Position;

            binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            await binaryDataBuilder.InitAsync(1);

            Assert.AreEqual(positionBefore, binaryDataBuilder.Position);
        }
        
        [Test]
        public async Task TestCaseWhenPackageGetsSeveralPages()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            

            var firstPackage = new byte[520];
            
            Array.Fill<byte>(firstPackage, 5);
            
            await binaryDataBuilder.AppendAsync(firstPackage);

            var array2 = new byte[] {4, 5, 6};
            await binaryDataBuilder.AppendAsync(array2);

            binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            var restoredItems = await binaryDataBuilder.InitAndReadAsync(1).ToListAsync();

           Assert.AreEqual(2, restoredItems.Count);


           var array3 = new byte[] {7, 8, 9};
           await binaryDataBuilder.AppendAsync(array3);
           
           binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
           
           restoredItems = await binaryDataBuilder.InitAndReadAsync(1).ToListAsync();
           
           Assert.AreEqual(3, restoredItems.Count);

           firstPackage.AssertItEqualsWith(restoredItems[0].ToArray());
           array2.AssertItEqualsWith(restoredItems[1].ToArray());
           array3.AssertItEqualsWith(restoredItems[2].ToArray());
           
        }
        
        
        [Test]
        public async Task TestCaseWhenPackageGetsSeveralPagesAndPageSizeIs2Pages()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 2);

            await binaryDataBuilder.InitAndReadAsync(2).ToListAsync();

            var firstPackage = new byte[1030];
            
            Array.Fill<byte>(firstPackage, 5);
            
            await binaryDataBuilder.AppendAsync(firstPackage);

            var array2 = new byte[] {4, 5, 6};
            await binaryDataBuilder.AppendAsync(array2);


            
            binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            var restoredItems = await binaryDataBuilder.InitAndReadAsync(2).ToListAsync();

            Assert.AreEqual(2, restoredItems.Count);


            var array3 = new byte[] {7, 8, 9};
            await binaryDataBuilder.AppendAsync(array3);
           
            binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
           
            restoredItems = await binaryDataBuilder.InitAndReadAsync(1).ToListAsync();
           
            Assert.AreEqual(3, restoredItems.Count);

            firstPackage.AssertItEqualsWith(restoredItems[0].ToArray());
            array2.AssertItEqualsWith(restoredItems[1].ToArray());
            array3.AssertItEqualsWith(restoredItems[2].ToArray());
           
        }
    }
}