using System;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.RandomAccess;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class RandomDataAccessShortCacheReadsTests
    {

        [Test]
        public async Task TestIntWithinThePage()
        {
            var azureBlob = new MyAzurePageBlobInMem();

            var randomAccess = new RandomAccessDataBuilder(azureBlob)
                .EnableCaching(512);
            
            var srcValue = 345678910;

            for (var i = 0; i < 511; i++)
            {

                await randomAccess.WriteIntAsync(srcValue, i);

                var bytes = randomAccess.PagesCache.TryGet(0, i, sizeof(int));

                var result = BitConverter.ToInt32(bytes.Span);
            
                Assert.AreEqual(srcValue, result);
            }


        }


        [Test]
        public async Task TestInt16Extension()
        {
            var azureBlob = new MyAzurePageBlobInMem();

            var randomAccess = new RandomAccessDataBuilder(azureBlob)
                .EnableCaching(512);


            const short srcValue = 12345;

            await randomAccess.WriteInt16Async(srcValue, 10);

            var result = await randomAccess.ReadInt16Async(10);
            
            Assert.AreEqual(srcValue, result);
        }
        
        [Test]
        public async Task TestUInt16Extension()
        {
            var azureBlob = new MyAzurePageBlobInMem();

            var randomAccess = new RandomAccessDataBuilder(azureBlob)
                .EnableCaching(512);

            const ushort srcValue = 65534;

            await randomAccess.WriteUInt16Async(srcValue, 10);

            var result = await randomAccess.ReadUInt16Async(10);
            
            Assert.AreEqual(srcValue, result);
        }
        

        [Test]
        public async Task TestInt32Extension()
        {
            var azureBlob = new MyAzurePageBlobInMem();

            var randomAccess = new RandomAccessDataBuilder(azureBlob)
                .EnableCaching(512);


            const int srcValue = 16383383;

            await randomAccess.WriteIntAsync(srcValue, 10);

            var result = await randomAccess.ReadIntAsync(10);
            
            Assert.AreEqual(srcValue, result);
        }
        
        [Test]
        public async Task TestUInt32Extension()
        {
            var azureBlob = new MyAzurePageBlobInMem();

            var randomAccess = new RandomAccessDataBuilder(azureBlob)
                .EnableCaching(512);


            const uint srcValue = 1638338343;

            await randomAccess.WriteUIntAsync(srcValue, 10);

            var result = await randomAccess.ReadUIntAsync(10);
            
            Assert.AreEqual(srcValue, result);
        }
        
        [Test]
        public async Task TestInt64Extension()
        {
            var azureBlob = new MyAzurePageBlobInMem();

            var randomAccess = new RandomAccessDataBuilder(azureBlob)
                .EnableCaching(512);


            const long srcValue = 1638338343456654;

            await randomAccess.WriteInt64Async(srcValue, 10);

            var result = await randomAccess.ReadInt64Async(10);
            
            Assert.AreEqual(srcValue, result);
        }
        
        [Test]
        public async Task TestUInt64Extension()
        {
            var azureBlob = new MyAzurePageBlobInMem();

            var randomAccess = new RandomAccessDataBuilder(azureBlob)
                .EnableCaching(512);


            const ulong srcValue = 1638338343456654;

            await randomAccess.WriteUInt64Async(srcValue, 10);

            var result = await randomAccess.ReadUInt64Async(10);
            
            Assert.AreEqual(srcValue, result);
        }
        
    }
}