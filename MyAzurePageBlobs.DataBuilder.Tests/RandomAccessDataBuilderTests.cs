using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.RandomAccess;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class RandomAccessDataBuilderTests
    {
        [Test]
        public async Task TestOnePageAllDataUseCase()
        {

            var blob = new MyAzurePageBlobInMem();

            var randomAccessDataBuilder = new RandomAccessDataBuilder(blob);

            var data = new byte[] {1, 2, 3};
            
            await randomAccessDataBuilder.WriteAsync(3, data);

            var resultData = await blob.DownloadAsReadOnlyMemoryAsync();

            resultData.Slice(3, 3).ToArray().AssertItEqualsWith(data);
            
            data = new byte[] {4, 5, 6};
            
            await randomAccessDataBuilder.WriteAsync(6, data);

            resultData = await blob.DownloadAsReadOnlyMemoryAsync();
            
            resultData.Slice(3, 6).ToArray().AssertItEqualsWith(new byte[]{1,2,3,4,5,6});
            
            
            data = new byte[] {4, 5, 6};
            
            await randomAccessDataBuilder.WriteAsync(515, data);

            resultData = await blob.DownloadAsReadOnlyMemoryAsync();
            
            resultData.Slice(515, 3).ToArray().AssertItEqualsWith(data);

        }

        [Test]
        public static async Task TestRandomWrites()
        {
            var blob = new MyAzurePageBlobInMem();


            var randomAccessDataBuilder = new RandomAccessDataBuilder(blob);

            var random = new Random();


            byte b = 0; 

            for (var i = 0; i < 10000; i++)
            {
                var nextOffset = random.Next(10000);

                var list = new List<byte> {b++, b++, b++};

                var data = list.ToArray();
            
                await randomAccessDataBuilder.WriteAsync(nextOffset, data);

                var resultData = await blob.DownloadAsReadOnlyMemoryAsync();
                
                resultData.Slice(nextOffset, data.Length).ToArray().AssertItEqualsWith(data);

            }


        }


        [Test]
        public async Task TestBigChunkWriteByPages()
        {
            var blob = new MyAzurePageBlobInMem();

            var randomAccessDataBuilder = new RandomAccessDataBuilder(blob, 1);

            var data = new byte[2000];

            for (var i = 0; i < data.Length; i++)
            {
                data[i] = (byte) i;
            }


            await randomAccessDataBuilder.WriteAsync(0, data);

            var result = await randomAccessDataBuilder.ReadAsync(0, data.Length);
            
            result.ToArray().AssertItEqualsWith(data);

        }
    }
}