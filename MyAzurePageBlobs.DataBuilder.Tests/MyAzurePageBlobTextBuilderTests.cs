using System;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.Text;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class MyAzurePageBlobTextBuilderTests
    {
        
        [SetUp]
        public void Init()
        {
            MyAzurePageBlobUtils.AllocateMemory = size => GC.AllocateUninitializedArray<byte>(size);
        }
        
        [Test]
        public async Task TestSimpleCase()
        {

            var pageBlob = new MyAzurePageBlobInMem();
            var textBuilder = new MyAzurePageBlobTextBuilder(pageBlob, 1);

            var testString = "My test";
            await textBuilder.AppendAsync(testString);

            var resultText = await textBuilder.DownloadAsync();
            
            Assert.AreEqual(testString, resultText);
        }


        [Test]
        public async Task TestPositionAfterEmptyBlobInit()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var textBuilder = new MyAzurePageBlobTextBuilder(pageBlob, 1);
            await textBuilder.InitAsync();

            Assert.AreEqual(0, textBuilder.Position);
        }

        [Test]
        public async Task TestPositionAfterRestoreIsTheSame()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var textBuilder = new MyAzurePageBlobTextBuilder(pageBlob, 1);
            
            var testString = "My test";
            await textBuilder.AppendAsync(testString);
            var position = textBuilder.Position;
            
            var textBuilder2 = new MyAzurePageBlobTextBuilder(pageBlob, 1);

            await textBuilder2.InitAsync();
            
            Assert.AreEqual(position, textBuilder2.Position);
        }
        
        [Test]
        public async Task TestReloadCase()
        {

            var pageBlob = new MyAzurePageBlobInMem();
            var textBuilder = new MyAzurePageBlobTextBuilder(pageBlob, 1);

            var testString = "My test";
            await textBuilder.AppendAsync(testString);

            var resultText = await textBuilder.DownloadAsync();
            Console.WriteLine("String after the First Operation");
            Console.WriteLine(resultText);
            
 
            // Now we load Blob To the new TextBuilder and Append a String

            textBuilder = new MyAzurePageBlobTextBuilder(pageBlob, 1);
            await textBuilder.AppendAsync(testString);
            resultText = await textBuilder.DownloadAsync();
            Console.WriteLine("String after the Second Operation");
            Console.WriteLine(resultText);
            
            Assert.AreEqual(resultText, testString + testString);
        } 
        
        

    }
}