using System;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.PagesCaching;
using MyAzurePageBlobs.DataBuilder.RandomAccess;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class RandomAccessDataBuilderWithCacheTests
    {

        /// <summary>
        /// What do we have in cache
        /// [-][1][-][2][-]
        /// What do we have in blob
        /// [0][1][2][3][4]
        /// Assert - we read pages from 1 with amount of 4 and get data mixed from Cache and Blob
        /// </summary>
        [Test]
        public async Task TestCompilingDataFromCacheAndFromBlob()
        {
            
            //FillingBlobWithData
            var azureBlob = new MyAzurePageBlobInMem();
            await azureBlob.FillPageWithByteAsync(2, 2);
            await azureBlob.FillPageWithByteAsync(4, 4);

            var randomAccess = new RandomAccessDataBuilder(azureBlob);

            randomAccess.EnableCaching(10);
            
            //Filling cache
               
            const int pageCached1 = 1;
            randomAccess.PagesCache.FillPageWithByte(pageCached1, 1);

            const int pageCached2 = 3;
            randomAccess.PagesCache.FillPageWithByte(pageCached2, 3);

            //Reading result

            var resultBuffer = await randomAccess.ReadAsync(512, 512 * 4);
            
            Assert.AreEqual(512*4, resultBuffer.Length);
            
            
            Assert.AreEqual(1, resultBuffer.Span[0]);




        }
        
        
    }
}