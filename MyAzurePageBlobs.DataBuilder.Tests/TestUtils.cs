using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.Json;
using MyAzurePageBlobs.DataBuilder.PagesCaching;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public static class TestUtils
    {
        
        public static MemoryStream ToMemoryStream(this byte[] src)
        {
            var result = new MemoryStream();
            result.Write(src);
            result.Position = 0;
            return result;
        }


        public static void AssertItEqualsWith(this byte[] src, byte[] dest)
        {
            Assert.AreEqual(src.Length, dest.Length);
            for (var i = 0; i < src.Length; i++)
                Assert.AreEqual(src[i], dest[i]);
        }

        public static byte[] CreateArrayOfByte(byte b, int size)
        {
            var result = new byte[size];
            for (var i = 0; i < size; i++)
            {
                result[i] = b;
            }

            return result;
        }


        public static void FillWith(this Memory<byte> mem, byte b)
        {
            var span = mem.Span;
            for (var i = 0; i < mem.Length; i++)
                span[i] = b;
        }



        public static byte[] CreatePageToWrite(this byte[] data)
        {
            var blobSize = MyAzurePageBlobUtils.CalculateRequiredBlobSize(0, data.Length, 1);
            var result = new byte[blobSize];
            Array.Copy(data, 0, result, 0, data.Length);
            return result;
        }


        public static byte[] Append(this byte[] data, params byte[] newData)
        {
            var memoryStream = new MemoryStream(data.Length + newData.Length);
            memoryStream.Write(data);
            memoryStream.Write(newData);
            return memoryStream.ToArray();
        }

        public static async Task<byte[]> AsArrayAsync(this ValueTask<MemoryStream> memoryStreamTask)
        {
            var stream = await memoryStreamTask;
            return stream.ToArray();
        }


        public static async ValueTask<IReadOnlyList<T>> ToListAsync<T>(this IAsyncEnumerable<T> src)
        {
            var result = new List<T>();
            await foreach (var itm in src)
            {
                result.Add(itm);
            }

            return result;
        }


        public static void FillPageWithByte(this PagesCache pagesCache, long pageNo, byte b)
        {
            var chunk = new byte[MyAzurePageBlobUtils.PageSize];
            Array.Fill(chunk, b);
            pagesCache.Add(pageNo, chunk);
        }
        
        
        public static async Task FillPageWithByteAsync(this IAzurePageBlob pageBlob, long pageNo, byte b)
        {
            var memoryStream = new MemoryStream();
            for (var i = 0; i < MyAzurePageBlobUtils.PageSize; i++)
                memoryStream.WriteByte(b);

            memoryStream.Position = 0;
            await pageBlob.WriteAsync(memoryStream, pageNo);
        }

        public static string GetFirstPartOfText(this string text, int maxSize = 64)
        {
            if (text == null)
                return "<<<NULL>>>";

            if (text.Length < maxSize)
                return text;

            return text.Substring(0, maxSize) + "...";
        }
        
        
        public static string GetLastPartOfText(this string text, int maxSize = 64)
        {
            if (text == null)
                return "<<<NULL>>>";

            if (text.Length < maxSize)
                return text;

            return  "..."+text.Substring(text.Length-64, maxSize);
        }
        
        public static string GetTheMiddleOfTheText(this string text, int middleIndex, int maxSize = 64)
        {
            if (text == null)
                return "<<<NULL>>>";

            if (text.Length < maxSize)
                return text;

            return  "..."+text.Substring(middleIndex-maxSize/2, maxSize)+"...";
        }



        public static MyAzurePageBlobJsonArrayBuilder WithNewtonSoftJsonSerializer(this MyAzurePageBlobJsonArrayBuilder builder)
        {
            return builder
                .SetJsonObjectSerializer((o, stream) =>
                {
                    var serializedJson = Encoding.ASCII.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(o));
                    stream.Write(serializedJson);
                });
        }
        
    }
}