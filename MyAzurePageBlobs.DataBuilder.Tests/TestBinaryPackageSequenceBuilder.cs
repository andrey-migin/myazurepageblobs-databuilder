using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyAzurePageBlobs.DataBuilder.BinaryPackagesSequence;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class TestBinaryPackageSequenceBuilder
    {
        
        [SetUp]
        public void Init()
        {
            MyAzurePageBlobUtils.AllocateMemory = size => GC.AllocateUninitializedArray<byte>(size);
        }
        
        
        [Test]
        public async Task TestSimpleCase()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            
            await binaryDataBuilder.AppendAsync(new byte[] {1, 2, 3});
            
            await binaryDataBuilder.AppendAsync(new byte[] {4, 5, 6});

            var array = await pageBlob.DownloadAsync().AsArrayAsync();

            var resultArray = new byte[] {3, 0, 0, 0, 1, 2, 3, 3, 0, 0, 0, 4, 5, 6, 0, 0, 0, 0};
            
            array.AsSpan(0,18).ToArray().AssertItEqualsWith(resultArray);
            
        }
        
        [Test]
        public async Task TestMultiArrayCase()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);

            var bulkInsertData = new List<ReadOnlyMemory<byte>>
            {
                new byte[] {1, 2, 3}, 
                new byte[] {4, 5, 6}
            };

            await binaryDataBuilder.AppendAsync(bulkInsertData);

            var data = await pageBlob.DownloadAsync();

            var array = data.ToArray();

            var resultArray = new byte[] {3, 0, 0, 0, 1, 2, 3, 3, 0, 0, 0, 4, 5, 6, 0, 0, 0, 0};
            
            array.AsSpan(0,18).ToArray().AssertItEqualsWith(resultArray);
            
        }
        
        
        //Fill the whole page of 512 bytes with the Frame.
        //After append operation - last FF FF FF FF must be rewritten and page ressized

        [Test]
        public async Task CheckIfWeFillFullPageAndTryingToAddTheNextOne()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);
            

            var payLoad = new byte[MyAzurePageBlobUtils.PageSize - 8];
            Array.Fill(payLoad,(byte)1);
            
            await binaryDataBuilder.AppendAsync(payLoad);
            var resultArray = await pageBlob.DownloadAsync().AsArrayAsync();
            
            
            Assert.AreEqual(512, resultArray.Length);
            
            Assert.AreEqual(1, resultArray[100]);
            
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-4]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-3]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-2]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-1]);
            
            await binaryDataBuilder.AppendAsync(new byte[] {1, 2, 3});
            
            resultArray = await pageBlob.DownloadAsync().AsArrayAsync();
            
            Assert.AreEqual(1024, resultArray.Length);
            
            Assert.AreEqual(3, resultArray[MyAzurePageBlobUtils.PageSize-4]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-3]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-2]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-1]);
            Assert.AreEqual(1, resultArray[MyAzurePageBlobUtils.PageSize]);
            Assert.AreEqual(2, resultArray[MyAzurePageBlobUtils.PageSize+1]);
            Assert.AreEqual(3, resultArray[MyAzurePageBlobUtils.PageSize+2]);
            
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+3]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+4]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+5]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+6]);
        }
        
        
        [Test]
        public async Task CheckIfWeFillFullPageAndEndMarkerWentPartiallyNextPage()
        {
            var pageBlob = new MyAzurePageBlobInMem();
            var binaryDataBuilder = new BinaryPackagesSequenceBuilder(pageBlob, 1024, 1);

            var payLoad = new byte[MyAzurePageBlobUtils.PageSize - 7];
            Array.Fill(payLoad,(byte)1);
            
            await binaryDataBuilder.AppendAsync(payLoad);
            var resultArray = await pageBlob.DownloadAsync().AsArrayAsync();
            
            
            Assert.AreEqual(1024, resultArray.Length);
            
            Assert.AreEqual(1, resultArray[100]);
            
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-3]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-2]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-1]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-0]);
            
            await binaryDataBuilder.AppendAsync(new byte[] {1, 2, 3});
            
            resultArray = await pageBlob.DownloadAsync().AsArrayAsync();
            
            Assert.AreEqual(1024, resultArray.Length);
            
            Assert.AreEqual(3, resultArray[MyAzurePageBlobUtils.PageSize-3]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-2]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-1]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize-0]);
            Assert.AreEqual(1, resultArray[MyAzurePageBlobUtils.PageSize+1]);
            Assert.AreEqual(2, resultArray[MyAzurePageBlobUtils.PageSize+2]);
            Assert.AreEqual(3, resultArray[MyAzurePageBlobUtils.PageSize+3]);
            
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+4]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+5]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+6]);
            Assert.AreEqual(0, resultArray[MyAzurePageBlobUtils.PageSize+7]);
        }        
        
        
        
    }
    
}