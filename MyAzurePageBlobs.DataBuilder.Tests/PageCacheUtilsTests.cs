using System.Collections.Generic;
using System.Linq;
using MyAzurePageBlobs.DataBuilder.PagesCaching;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class PageCacheUtilsTests
    {

        [Test]
        public void TestCombiningPages()
        {

            var missingPages = new List<long> {2,3,4,  7,8,  12,   15};

            var result = missingPages.CombineAndGetPagesToLoad().ToArray();
            
            Assert.AreEqual(4, result.Length);
            
            Assert.AreEqual(2, result[0].startPageToLoad);
            Assert.AreEqual(3, result[0].pagesAmountToLoad);
            
            Assert.AreEqual(7, result[1].startPageToLoad);
            Assert.AreEqual(2, result[1].pagesAmountToLoad);

            Assert.AreEqual(12, result[2].startPageToLoad);
            Assert.AreEqual(1, result[2].pagesAmountToLoad);


            Assert.AreEqual(15, result[3].startPageToLoad);
            Assert.AreEqual(1, result[3].pagesAmountToLoad);
        }
        
        [Test]
        public void TestCombiningPages2()
        {

            var missingPages = new List<long> {1, 3,4,5,6,  10,11,  17,   22};

            var result = missingPages.CombineAndGetPagesToLoad().ToArray();
            
            Assert.AreEqual(5, result.Length);
            
            Assert.AreEqual(1, result[0].startPageToLoad);
            Assert.AreEqual(1, result[0].pagesAmountToLoad);
            
            Assert.AreEqual(3, result[1].startPageToLoad);
            Assert.AreEqual(4, result[1].pagesAmountToLoad);

            Assert.AreEqual(10, result[2].startPageToLoad);
            Assert.AreEqual(2, result[2].pagesAmountToLoad);
            
            Assert.AreEqual(17, result[3].startPageToLoad);
            Assert.AreEqual(1, result[3].pagesAmountToLoad);
            
            Assert.AreEqual(22, result[4].startPageToLoad);
            Assert.AreEqual(1, result[4].pagesAmountToLoad);
            
        }
    }
}