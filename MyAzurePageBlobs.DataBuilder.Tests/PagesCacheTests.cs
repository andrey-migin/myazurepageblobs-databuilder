using System;
using MyAzurePageBlobs.DataBuilder.PagesCaching;
using NUnit.Framework;

namespace MyAzurePageBlobs.DataBuilder.Tests
{
    public class PagesCacheTests
    {
        /// <summary>
        /// What do we have in Cache:
        /// [-][1][-][2][-]
        /// What do we read
        ///     *  *  *  *
        /// [-][1][-][2][-]
        /// </summary>
        [Test]
        public void TestBasicUseCase()
        {

            var pagesCache = new PagesCache(10);
            
            
            const int pageCached1 = 1;
            pagesCache.FillPageWithByte(pageCached1, 1);

            const int pageCached2 = 3;
            pagesCache.FillPageWithByte(pageCached2, 2);


            var resultBuffer = new byte[MyAzurePageBlobUtils.PageSize*4];

            const int firstReadPage = 1;

            var missingPages = pagesCache.FillPagesWhichAreInCache(firstReadPage, 4, resultBuffer);
            
            Assert.AreEqual(2, missingPages.Count);
            
            Assert.AreEqual(1, resultBuffer[(pageCached1-firstReadPage)*MyAzurePageBlobUtils.PageSize]);
            
            Assert.AreEqual(2, resultBuffer[(pageCached2-firstReadPage)*MyAzurePageBlobUtils.PageSize]);
            
            Assert.AreEqual(2, missingPages[0]);
            Assert.AreEqual(4, missingPages[1]);

        }
        
    }
}